package by.kolovaitis.ksis.tools

import by.kolovaitis.ksis.viewModel.NetworkViewModel
import java.net.InetAddress
import java.net.Socket
import java.util.*

class TCPManager() {

    object Constants {
        const val DEFAULT_DELIMITER = "&"
        const val REFRESH_DELTA: Long = 100
        const val COMMON_DESTINATION = 0
        const val MY_ID = -1
        const val MESSAGE: Byte = 0
        const val ADD: Byte = 1
        const val REMOVE: Byte = 2
    }

    inner class Connection(
        private val address: InetAddress,
        private val port: Int,
        private val nickname: String,
        private val messageObserver: NetworkViewModel.MessageObserver,
        private val connectionStatusObserver: NetworkViewModel.ConnectionStatusObserver

    ) {
        private lateinit var socket: Socket
        private val lifecycleThread: Thread = Thread {
            connectionStatusObserver.connectionStatusHasChanged(ConnectionStatus.IN_PROCESS)
            socket = Socket(address, port)
            connectionStatusObserver.connectionStatusHasChanged(ConnectionStatus.CONNECTED)
            val scanner = Scanner(socket.getInputStream())
            sendMessage(nickname)
            while (isAlive()) {
                if (scanner.hasNext()) {
                    messageObserver.messageReceived(parseMessage(scanner.nextLine()))
                } else {
                    Thread.sleep(Constants.REFRESH_DELTA)
                }
            }
            connectionStatusObserver.connectionStatusHasChanged(ConnectionStatus.DISCONNECTED)
        }

        private fun parseMessage(value: String): Message {
            val fields = value!!.split(Constants.DEFAULT_DELIMITER)
            val type = fields[0].toByte()
            val id = fields[1].toInt()
            val ip = fields[2]
            val timeMillis = fields[3].toLong()
            val nickname = fields[4]
            val info = fields[5]
            val isPrivate = fields[6].toBoolean()
            return Message(type, id, ip, timeMillis, nickname, info, isPrivate)
        }

        fun start() {
            lifecycleThread.start()
        }


        fun sendMessage(value: String, destination: Int = Constants.COMMON_DESTINATION) {
            val outputStream = socket.getOutputStream()
            outputStream
                .write("$value${Constants.DEFAULT_DELIMITER}$destination${Constants.DEFAULT_DELIMITER}\n".toByteArray())
            outputStream.flush()
        }

        fun isAlive(): Boolean {
            return !socket.isClosed
        }

        fun stop() {
            Thread { socket.close() }.start()
        }
    }
}