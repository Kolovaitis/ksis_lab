package by.kolovaitis.ksis.ui

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import by.kolovaitis.ksis.R
import by.kolovaitis.ksis.tools.Message
import by.kolovaitis.ksis.tools.TCPManager
import by.kolovaitis.ksis.viewModel.NetworkViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        nickname.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                textChanged()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })
        button_connect.setOnClickListener {
            startActivity(Intent(this, ChatActivity::class.java).apply {
                putExtra(
                    "nickname",
                    nickname.text.toString()
                )
            })
        }
    }

    fun textChanged() {
        button_connect.isEnabled =
            !(nickname.text.toString() == "" || nickname.text.toString().contains(TCPManager.Constants.DEFAULT_DELIMITER))
    }



}
