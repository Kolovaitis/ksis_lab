package by.kolovaitis.ksis.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import by.kolovaitis.ksis.tools.*


class NetworkViewModel : ViewModel() {

    private val aliveUsers: MutableList<User> = mutableListOf()
    private val aliveUsersObservable: MutableLiveData<List<User>> by lazy {
        MutableLiveData<List<User>>()
    }
    private lateinit var tcpConnection: TCPManager.Connection
    private var currentDestination = TCPManager.Constants.COMMON_DESTINATION
    private val commonMessagesList: MutableList<Message> = mutableListOf()
    private val privateMessagesMap: MutableMap<Int, MutableList<Message>> = mutableMapOf()
    private val connectionStatus: MutableLiveData<ConnectionStatus> by lazy {
        MutableLiveData<ConnectionStatus>().apply {
            this.postValue(
                ConnectionStatus.IN_PROCESS
            )
        }
    }
    private val newPrivateMessage: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }
    private val messages: MutableLiveData<List<Message>> by lazy {
        MutableLiveData<List<Message>>()
    }

    inner class MessageObserver {
        fun messageReceived(message: Message) {
            if (message.isPrivate) {
                privateMessagesMap.getOrPut(message.idUser) { mutableListOf() }.add(message)
                postValue(privateMessagesMap[message.idUser], message.idUser, true)
            } else {
                commonMessagesList.add(message)
                postValue(commonMessagesList, isMessage = true)
                when (message.type) {
                    TCPManager.Constants.ADD -> {
                        privateMessagesMap.getOrPut(message.idUser) { mutableListOf() }.add(message)
                        postValue(privateMessagesMap[message.idUser], message.idUser)
                        aliveUsers.add(User(message.idUser, message.nickname))
                        aliveUsersObservable.postValue(aliveUsers)
                    }
                    TCPManager.Constants.REMOVE -> {
                        privateMessagesMap.getOrPut(message.idUser) { mutableListOf() }.add(message)
                        postValue(privateMessagesMap[message.idUser], message.idUser)
                        aliveUsers.remove(aliveUsers.find { user -> user.id == message.idUser })
                        aliveUsersObservable.postValue(aliveUsers)
                    }
                }
            }
        }
    }

    private fun postValue(
        list: List<Message>?,
        destination: Int = TCPManager.Constants.COMMON_DESTINATION,
        isMessage: Boolean = false
    ) {
        if (destination == currentDestination) {
            messages.postValue(list)
        } else {
            if (isMessage)
                newPrivateMessage.postValue(destination)
        }
    }

    inner class ConnectionStatusObserver {
        fun connectionStatusHasChanged(status: ConnectionStatus) {
            connectionStatus.postValue(status)
        }
    }


    fun getConnectionStatus(): LiveData<ConnectionStatus> {
        return connectionStatus
    }

    fun connect(nickname: String) {
        Thread {
            val udpConnection = UDPManager().Connection()
            val serverCredentials = udpConnection.findServer()
            udpConnection.close()
            tcpConnection = TCPManager().Connection(
                serverCredentials.first,
                serverCredentials.second,
                nickname,
                MessageObserver(),
                ConnectionStatusObserver()
            )
            tcpConnection.start()
        }.start()
    }

    fun disconnect() {
        Thread {
            tcpConnection.stop()
        }.start()
        commonMessagesList.clear()
        privateMessagesMap.clear()

    }


    private fun sendPrivateMessage(value: String, destination: Int) {
        Thread { tcpConnection.sendMessage(value, destination) }.start()
        val message = Message(
            TCPManager.Constants.MESSAGE,
            TCPManager.Constants.MY_ID,
            NetworkHelper.localIp!!.hostAddress,
            System.currentTimeMillis(),
            "",
            value
        )
        if (destination == TCPManager.Constants.COMMON_DESTINATION) {
            commonMessagesList.add(message)
            postValue(commonMessagesList)
        } else {
            privateMessagesMap[destination]?.add(message)
            postValue(
                privateMessagesMap[destination], destination
            )
        }
    }

    fun getMessages(): LiveData<List<Message>> {
        return messages
    }

    fun sendMessage(value: String) {
        sendPrivateMessage(value, currentDestination)
    }

    fun setCurrentDestination(destination: Int) {
        currentDestination = destination
        messages.postValue(
            if (destination == TCPManager.Constants.COMMON_DESTINATION)
                commonMessagesList
            else
                privateMessagesMap[destination]
        )
    }

    fun getNewPrivateMessage(): LiveData<Int> {
        return newPrivateMessage
    }

    fun getCurrentDestination(): Int {
        return currentDestination
    }
    fun getAliveUsers():LiveData<List<User>>{
        return aliveUsersObservable
    }
}
