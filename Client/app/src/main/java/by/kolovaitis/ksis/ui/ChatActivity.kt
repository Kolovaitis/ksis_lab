package by.kolovaitis.ksis.ui

import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import by.kolovaitis.ksis.R
import by.kolovaitis.ksis.tools.ConnectionStatus
import by.kolovaitis.ksis.tools.Message
import by.kolovaitis.ksis.tools.TCPManager
import by.kolovaitis.ksis.viewModel.NetworkViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.chat_activity.*


class ChatActivity : AppCompatActivity(), MessageFragment.OnListFragmentInteractionListener {
    val model: NetworkViewModel by viewModels()
    var currentDestination = TCPManager.Constants.COMMON_DESTINATION

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.chat_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.container,
                    MessageFragment()
                )
                .commitNow()
        }
        model.connect(intent.getStringExtra("nickname"))
        model.getConnectionStatus().observe(this, Observer {
            when (it) {
                ConnectionStatus.DISCONNECTED -> {
                    Toast.makeText(this, getString(R.string.connection_lost), Toast.LENGTH_SHORT)
                        .show()
                    finish()
                }
                ConnectionStatus.IN_PROCESS -> progressbar_layout.visibility = View.VISIBLE
                ConnectionStatus.CONNECTED -> progressbar_layout.visibility = View.GONE
            }
        })
        model.getNewPrivateMessage().observe(this, Observer {
            val destination = it
            val snackbar: Snackbar = Snackbar
                .make(container, getString(R.string.new_message), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.view), View.OnClickListener {
                    model.setCurrentDestination(destination)
                })
            snackbar.setActionTextColor(getColor(R.color.colorRed))

            snackbar.show()
        })
        model.getMessages().observe(this, Observer {
            supportActionBar?.title =
                if (model.getCurrentDestination() == TCPManager.Constants.COMMON_DESTINATION)
                    getString(R.string.common) else getString(R.string.private_chat)
        })
    }

    override fun onBackPressed() {
        model.setCurrentDestination(TCPManager.Constants.COMMON_DESTINATION)

    }


    override fun onListFragmentInteraction(item: Message?) {
        model.setCurrentDestination(item!!.idUser)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        model.getAliveUsers().observe(this, Observer {
            menu!!.clear()
            for (i in it.indices) {
                menu!!.add(Menu.NONE, it[i].id, Menu.NONE, it[i].name)
            }
        })
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item:MenuItem):Boolean
    {
        model.setCurrentDestination(item.itemId)
        return true
    }
}
