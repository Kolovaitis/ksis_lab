package by.kolovaitis.ksis.tools

enum class ConnectionStatus {
    IN_PROCESS, CONNECTED, DISCONNECTED
}
