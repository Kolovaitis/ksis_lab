package by.kolovaitis.ksis.ui

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnChildAttachStateChangeListener
import by.kolovaitis.ksis.R
import by.kolovaitis.ksis.tools.TCPManager
import by.kolovaitis.ksis.viewModel.NetworkViewModel
import kotlinx.android.synthetic.main.fragment_message_list.*
import kotlinx.android.synthetic.main.fragment_message_list.view.*
import java.lang.IllegalArgumentException


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [MessageFragment.OnListFragmentInteractionListener] interface.
 */
class MessageFragment : Fragment() {
    private val model: NetworkViewModel by activityViewModels()


    // TODO: Customize parameters
    private var columnCount = 1

    private var listener: OnListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_message_list, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.list)
        // Set the adapter
        if (recyclerView is RecyclerView) {
            with(recyclerView) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = MyMessageRecyclerViewAdapter(
                    emptyList(),
                    listener
                )
                model.getMessages().observe(viewLifecycleOwner, Observer {
                    (adapter as MyMessageRecyclerViewAdapter).refresh(it)
                    smoothScrollToPosition(it.size - 1)
                })
                addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
                    if (bottom < oldBottom)
                        try {
                            smoothScrollToPosition((adapter as MyMessageRecyclerViewAdapter).itemCount - 1)

                        } catch (e: IllegalArgumentException) {
                        }
                }

            }

        }
        view.message_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val text = message_text.text.toString()
                view.button_send.isEnabled =
                    !(text == "" || text.contains(TCPManager.Constants.DEFAULT_DELIMITER))
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
        view.button_send.setOnClickListener {
            model.sendMessage(message_text.text.toString())
            view.message_text.text.clear()
        }
        try {
            recyclerView.smoothScrollToPosition((recyclerView.adapter as MyMessageRecyclerViewAdapter).itemCount - 1)

        } catch (e: IllegalArgumentException) {
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: by.kolovaitis.ksis.tools.Message?)
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            MessageFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}
