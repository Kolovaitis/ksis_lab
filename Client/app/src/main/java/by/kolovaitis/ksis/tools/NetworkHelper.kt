package by.kolovaitis.ksis.tools

import java.net.InetAddress
import java.net.NetworkInterface
import java.net.SocketException


object NetworkHelper {
    private val _localIp: InetAddress? by lazy {
        var ip: InetAddress? = null
        try {
            val interfaces =
                NetworkInterface.getNetworkInterfaces()
            while (interfaces.hasMoreElements()) {
                val networkInterface = interfaces.nextElement()
                if (networkInterface.isLoopback || !networkInterface.isUp) continue
                val addresses =
                    networkInterface.inetAddresses
                while (addresses.hasMoreElements()) {
                    val addr = addresses.nextElement()
                    if (addr.isSiteLocalAddress) {
                        ip = addr
                    }
                }
            }
        } catch (e: SocketException) {
            throw RuntimeException(e)
        }
        ip
    }
    val localIp:InetAddress?
        get():InetAddress? = _localIp

}