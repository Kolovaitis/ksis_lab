package by.kolovaitis.ksis.tools

data class Message(val type:Byte, val idUser:Int, val ip:String, val timeMillis:Long, val nickname:String, val value:String, val isPrivate:Boolean = false) {
}