package by.kolovaitis.ksis.ui


import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import by.kolovaitis.ksis.R
import by.kolovaitis.ksis.tools.Message
import by.kolovaitis.ksis.tools.TCPManager
import by.kolovaitis.ksis.ui.MessageFragment.OnListFragmentInteractionListener
import kotlinx.android.synthetic.main.fragment_message_connection.view.*
import java.text.SimpleDateFormat
import java.util.*


/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class MyMessageRecyclerViewAdapter(
    private var mValues: List<Message>,
    private val mListener: OnListFragmentInteractionListener?
) : RecyclerView.Adapter<MyMessageRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Message
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_message_connection, parent, false)
        return ViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mContent.text = item.value
        val sdf =
            SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss", Locale.US)

        holder.mIpTime.text = item.ip+", "+ sdf.format(item.timeMillis)
        val marginTop = (holder.mCloud.layoutParams as LinearLayout.LayoutParams).topMargin
        val marginBottom = marginTop
        var marginRight = marginTop
        var marginLeft = marginTop
        holder.mNickname.visibility = View.GONE
        holder.mView.setOnClickListener { }
        if (item.type == TCPManager.Constants.MESSAGE) {
            if (item.idUser != TCPManager.Constants.MY_ID) {
                holder.mNickname.text = item.nickname
                holder.mNickname.visibility = View.VISIBLE
                marginRight *= 24
                holder.mCloud.setCardBackgroundColor(holder.mView.context.getColor(R.color.colorPrimary))
            } else {
                holder.mCloud.setCardBackgroundColor(holder.mView.context.getColor(R.color.colorAccent))
                marginLeft *= 24
            }
        } else {
            marginRight *= 8
            marginLeft *= 8
            when (item.type) {
                TCPManager.Constants.ADD -> {
                    holder.mContent.text =
                        item.nickname + " " + holder.mView.context.getString(R.string.add)
                    holder.mCloud.setCardBackgroundColor(holder.mView.context.getColor(R.color.colorGreen))
                    holder.mView.setOnClickListener(mOnClickListener)

                }
                TCPManager.Constants.REMOVE -> {
                    holder.mContent.text =
                        item.nickname + " " + holder.mView.context.getString(R.string.remove)
                    holder.mCloud.setCardBackgroundColor(holder.mView.context.getColor(R.color.colorRed))
                }
            }
        }
        (holder.mCloud.layoutParams as LinearLayout.LayoutParams).setMargins(
            marginLeft,
            marginTop,
            marginRight,
            marginBottom
        )

        with(holder.mView) {
            tag = item
        }
    }

    override fun getItemCount(): Int = mValues.size
    fun refresh(messages: List<Message>) {
        if (messages.size == mValues.size + 1 && messages.containsAll(mValues)) {
            mValues = messages
            notifyItemInserted(messages.size - 1)
        } else {
            mValues = messages
            notifyDataSetChanged()
        }

    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mNickname: TextView = mView.nickname
        val mContent: TextView = mView.content
        val mCloud: CardView = mView.cloud
        val mIpTime: TextView = mView.ipTime
    }
}
