import java.io.Closeable
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

class UDPManager() {
    companion object {
        const val DEFAULT_BROADCAST_DESTINATION = "255.255.255.255"
        const val DEFAULT_UDP_PORT = 9000
        const val DEFAULT_DATA_SIZE = 512
    }

    private var broadcastDestination = DEFAULT_BROADCAST_DESTINATION
    private var udpPort = DEFAULT_UDP_PORT
    private val localIp: InetAddress by lazy {
        NetworkHelper.getLocalIP()
    }

    fun setBroadcastDestination(broadcastDestination: String): UDPManager {
        this.broadcastDestination = broadcastDestination
        return this
    }

    fun setUDPPort(udpPort: Int): UDPManager {
        this.udpPort = udpPort
        return this
    }

    inner class Connection() : Closeable {
        private val udpSocket: DatagramSocket by lazy {
            DatagramSocket()
        }
        val localUdpPort: Int
            get() = udpSocket.localPort

        fun findServer(): Pair<InetAddress, Int> {
            sendUDPString("${localIp.hostAddress}:$localUdpPort")
            val response = getUDPString().split(":")
            val port = response[1]
            println(port)
            return Pair(InetAddress.getByName(response[0]), port.toInt())
        }

        fun sendUDPString(value: String) {
            val data = value.toByteArray()
            val packet = DatagramPacket(data, data.size, InetAddress.getByName(broadcastDestination), udpPort)
            udpSocket.send(packet)
        }

        fun getUDPString(): String {
            var data = ByteArray(DEFAULT_DATA_SIZE)
            val packet = DatagramPacket(data, DEFAULT_DATA_SIZE)
            udpSocket.receive(packet)
            return String(data, 0, packet.length)
        }

        override fun close() {
            udpSocket.close()
        }
    }

}