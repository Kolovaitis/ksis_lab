import java.net.Socket

fun main() {
    val connection = UDPManager().Connection()
    val serverCredentials = connection.findServer()
    connection.close()
    val socket = Socket(serverCredentials.first, serverCredentials.second)
}